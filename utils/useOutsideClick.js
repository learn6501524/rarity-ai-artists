import { useEffect, useRef, useState } from 'react';

const useOutsideClick = () => {
  const ref = useRef();
  const [clickOutside, setClickOutside] = useState(false);

  useEffect(() => {
    const handleClick = (event) => {
      if (ref && ref.current && !ref.current.contains(event.target)) {
        setClickOutside(true);
      } else {
        setClickOutside(false);
      }
    };

    document.addEventListener('click', handleClick, true);

    return () => {
      document.removeEventListener('click', handleClick, true);
    };
  }, [ref]);

  return { ref, clickOutside };
};

export default useOutsideClick;
