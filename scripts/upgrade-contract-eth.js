const { ethers, upgrades } = require("hardhat");
//the address of the deployed proxy
const PROXY = "0xC3CA99B69e2F5528416D5d62FDa196674F861F39";

async function main() {
    const StarmintETH = await ethers.getContractFactory("StarmintEth");

    await upgrades.upgradeProxy(PROXY, StarmintETH);
    console.log("Starmint ETH contract upgraded");
}

main();