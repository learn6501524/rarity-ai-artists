const { ethers, upgrades } = require("hardhat");
//the address of the deployed proxy
const PROXY = "0x7916db850451D1c2832F0E809d44bc8bE00A00e3";

async function main() {
    const Starmint = await ethers.getContractFactory("Starmint");

    await upgrades.upgradeProxy(PROXY, Starmint);
    console.log("Starmint contract upgraded");
}

main();